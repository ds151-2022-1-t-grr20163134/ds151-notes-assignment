import React, { createContext, useReducer, useState } from "react"

const NotesContext = createContext()

const notesReducer = (state, action) => {
    let tempState = state;
    switch (action.type) {
        case 'create':
            tempState.lastId++
            tempState.notesQtd++
            tempState.notes.push({ id: tempState.lastId, content: action.payload })
            return { ...state, tempState }
        case 'delete':
            const delIndex = tempState.notes.map(x => x.id).indexOf(action.payload)
            tempState.notesQtd--
            tempState.notes.splice(delIndex, 1)
            return { ...state, tempState }
        case 'update':
            const updateIndex = tempState.notes.map(x => x.id).indexOf(action.payload.id)
            tempState.notes[updateIndex].content = action.payload.noteContent
            // 'notes: [...tempState.notes]' is used to tell JS that there's a difference in the array, otherwise it will only look for the reference(that didin´t changed) 
            return { ...state, notes: [...tempState.notes] }
        default:
            return { ...state }
    }
}

const NotesProvider = ({ children }) => {

    const [state, dispatch] = useReducer(notesReducer, { notes: [], notesQtd: 0, lastId: 0 })

    const createNote = (noteContent) => {
        dispatch({ type: 'create', payload: noteContent })
    }

    const updateNote = (id, noteContent) => {
        dispatch({ type: 'update', payload: { id: id, noteContent: noteContent } })
    }

    const deleteNote = (id) => {
        dispatch({ type: 'delete', payload: id })
    }

    return (
        <NotesContext.Provider value={{
            state,
            createNote,
            updateNote,
            deleteNote
        }}>
            {children}
        </NotesContext.Provider>
    )
}

export { NotesContext, NotesProvider }