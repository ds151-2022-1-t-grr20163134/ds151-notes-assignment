import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";

const Note = ({ note, handleEdit, handleDel }) => {
    return (
        <View style={styles.container}>
            <Text>{note.content}</Text>
            <View style={styles.btnContainer}>
                <TouchableOpacity onPress={() => handleEdit(note.id, note.content)}>
                    <Image style={styles.icon} source={require('../../assets/ic_edit.png')} />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => handleDel(note.id)}>
                    <Image style={styles.icon} source={require('../../assets/ic_del.png')} />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: '#eee'
    },
    btnContainer: {
        flexDirection: 'row',
        marginHorizontal: 20
    },
    icon: {
        height: 15,
        width: 15,
        marginLeft: 10
    }
})

export default Note

