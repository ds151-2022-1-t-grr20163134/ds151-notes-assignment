import React, { useContext, useEffect } from "react"
import { Button, FlatList, StyleSheet, Text, View } from "react-native"
import Note from "../components/Note"
import { NotesContext } from "../context/NotesContext"

const Dashboard = ({ navigation }) => {

    const { state, createNote, deleteNote } = useContext(NotesContext)

    useEffect(() => {
        // console.log(state.notes)
        // if (state.notes.length > 0) {
        //     console.log(state.notes[0].content)
        // }
    }, [state])

    useEffect(() => {
        createNote('teste A')
        createNote('teste B')
        createNote('teste C')
    }, [])

    const editNote = (id, note) => {
        navigation.navigate('Update Note', { id: id, note: note })
    }

    const handleDeleteNote = (id) => {
        deleteNote(id)
    }

    return (
        <View style={styles.container}>
            <Text>This is Dashboard!</Text>
            <View style={styles.notesContainer}>
                {state.notes.length > 0 ?
                    <View>
                        <Text>Notes:</Text>
                        <FlatList
                            data={state.notes}
                            keyExtractor={(item) => item.id.toString()}
                            renderItem={({ item }) => <Note note={item} handleEdit={editNote} handleDel={handleDeleteNote} />}
                        />
                    </View>
                    :
                    <Text>Add a new note.</Text>}
            </View>
            <Button title="Add note" onPress={() => navigation.navigate('Create Note')} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        display: "flex",
        justifyContent: 'space-between',
        height: '100%',
        width: '100%',
        backgroundColor: '#ddd',
        padding: 20,
    },
    notesContainer: {
        height: '90%',
    }
})

export default Dashboard