import React, { useContext, useEffect, useState } from "react"
import { Button, Text, TextInput, View } from "react-native"
import { NotesContext } from "../context/NotesContext"

const UpdateNote = ({ navigation, route }) => {

    const [note, setNote] = useState(null)
    const id = route.params.id
    const { updateNote } = useContext(NotesContext)

    useEffect(() => {
        setNote(route.params.note)
    }, [])

    const handleCreateNoteButton = () => {
        if (note == "" || note == undefined || note == null) {
            console.log("invalid note!")
            return
        } else {
            updateNote(id, note)
            navigation.navigate('Dashboard')
        }
    }
    if (note !== null) {
        return (
            <View>
                <TextInput
                    style={{ height: 40 }}
                    onChangeText={newNote => setNote(newNote)}
                    value={note}
                />
                <Button title="Create"
                    onPress={() => handleCreateNoteButton()} />
            </View>
        )
    } else {
        return (
            <Text>Carregando!</Text>
        )
    }
}

export default UpdateNote