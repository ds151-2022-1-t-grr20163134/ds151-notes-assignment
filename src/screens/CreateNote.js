import React, { useContext, useEffect, useState } from "react"
import { Button, TextInput, View } from "react-native"
import { NotesContext } from "../context/NotesContext"

const CreateNote = ({navigation}) => {

    const [note, setNote] = useState('')
    const { createNote } = useContext(NotesContext)

    const handleCreateNoteButton = () => {
        if (note == "" || note == undefined || note == null) {
            console.log("Note must have some content!")
            return
        } else {
            createNote(note)
            navigation.navigate('Dashboard')
        }
    }

    return (
        <View>
            <TextInput
                style={{ height: 40 }}
                placeholder="Add a new note..."
                onChangeText={newNote => setNote(newNote)}
                defaultValue={note}
            />
            <Button title="Create"
                onPress={() => handleCreateNoteButton()} />
        </View>
    )
}

export default CreateNote