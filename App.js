import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import { StyleSheet } from 'react-native';
import Dashboard from './src/screens/Dashboard';
import { NotesProvider } from './src/context/NotesContext'
import CreateNote from './src/screens/CreateNote';
import UpdateNote from './src/screens/UpdateNote';

const Stack = createNativeStackNavigator()

export default function App() {
  return (
    <NotesProvider>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name='Dashboard' component={Dashboard} />
          <Stack.Screen name='Create Note' component={CreateNote} />
          <Stack.Screen name='Update Note' component={UpdateNote} />
        </Stack.Navigator>
      </NavigationContainer>
    </NotesProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
